# Modify the variables below to match your project.

# This specifies the main site name in provisioning.
site_name='Example Site'

# This sets up the name of the DB and the user and password for the DB.
database='example_db'
dbuser='wp'
dbpass='wp'

# The site details for this site
domain='http://example.dev'
admin_user='admin'
admin_pass='password'
admin_email='jqpublic@gmail.com'